<?php

/**
 * @file
 * The controller for the installment entity containing the CRUD operations.
 */

/**
 * The controller class for installment contains methods for the order CRUD
 * operations. The load method is inherited from the default controller.
 */
class CommerceOrderInstallmentEntityController extends DrupalCommerceEntityController {

  /**
   * Create a default installment.
   *
   * @param array $values
   *   An array of values to set, keyed by property name
   *
   * @return
   *   An installment object with all default fields initialized.
   */
  public function create(array $values = array()) {
    return (object) ($values + array(
      'installment_id' => '',
      'installment_number' => '',
      'type' => 'commerce_order_installment',
      'uid' => '',
      'order_id' => 0,
      'created' => '',
      'changed' => '',
      'amount' => 0,
      'currency' => '',
      'due_date' => 0,
      'paid_date' => Null,
      'status' => '',
      'notes' => '',
      'transaction_id' => '',
      'added_by' => '',

      
    
      
    ));
  }

  /**
   * Saves an installment.
   *
   * When saving an installment, the function will automatically create an installment number
   * based 
   *
   * @param $installment
   *   The full installment object to save.
   * @param $transaction
   *   An optional transaction object.
   *
   * @return
   *   The saved installment object.
   */
  public function save($installment, DatabaseTransaction $transaction = NULL) {
    try {
      $installment->changed = REQUEST_TIME;
      
      // Inserting new installment
      if (empty($installment->installment_id)) {
        $installment->created = REQUEST_TIME;

        $this->invoke('presave', $installment);
        $installment = $this->_save($installment, $transaction);
        $this->invoke('insert', $installment);
      }
      else {
        $this->invoke('presave', $installment);
        $installment = $this->_save($installment, $transaction);
        $this->invoke('insert', $installment);
      }
      
      return $installment;
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * Saves an installment (helper function)
   *
   * @param $installment
   *   The full installment object to save.
   * @param $transaction
   *   An optional transaction object.
   *
   * @return
   *   The saved installment object.
   */
  private function _save($installment, DatabaseTransaction $transaction = NULL) {
    $transaction = isset($transaction) ? $transaction : db_transaction();
    try {
      if (empty($installment->installment_id)) {
        // Save the new installment
        drupal_write_record('commerce_order_installment', $installment);
        field_attach_insert('commerce_order_installment', $installment);
      }
      else {
        drupal_write_record('commerce_order_installment', $installment, 'installment_id');
        field_attach_update('commerce_order_installment', $installment);
      }
      // Ignore slave server temporarily to give time for the
      // saved installment to be propagated to the slave.
      db_ignore_slave();


      return $installment;
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('commerce_order_installment', $e);
      throw $e;
    }
  }


  
  /**
   * Deletes multiple installment by ID.
   *
   * @param $installment_ids
   *   An array of installment IDs to delete.
   * @param $transaction
   *  An optional transaction object.
   *
   * @return
   *   TRUE on success, FALSE otherwise.
   */
  public function delete($installment_ids, DatabaseTransaction $transaction = NULL) {
    if (!empty($installment_ids)) {
      $installment = $this->load($installment_ids, array());

      $transaction = isset($transaction) ? $transaction : db_transaction();

      try {
        db_delete('commerce_order_installment')
          ->condition('installment_id', $installment_ids, 'IN')
          ->execute();

        // Reset the cache as soon as the changes have been applied.
        $this->resetCache($installment_ids);
 
        foreach ($installment as $id => $installment) {
          $this->invoke('delete', $installment);
        }

        // Ignore slave server temporarily to give time for the
        // saved installment to be propagated to the slave.
        db_ignore_slave();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('commerce_order_installment', $e);
        throw $e;
      }

      // Clear the page and block and line_item_load_multiple caches.
      cache_clear_all();
      $this->resetCache();
    }

    return TRUE;
  }
  
  /**
   * Builds a structured array representing the entity's content.
   *
   * The content built for the entity will vary depending on the $view_mode
   * parameter.
   *
   * @param $entity
   *   An entity object.
   * @param $view_mode
   *   View mode, e.g. 'administrator'
   * @param $langcode
   *   (optional) A language code to use for rendering. Defaults to the global
   *   content language of the current request.
   * @return
   *   The renderable array.
   */
  public function buildContent($installment, $view_mode = 'administrator', $langcode = NULL, $content = array()) {
    // Load the order this installment is attached to.
    $order = commerce_order_load($installment->order_id);
    
    $content['installment_number'] = array(
      '#type' => 'item',
      '#title' => t('Installment number'),
      '#markup' => $installment->installment_number,
    );
    
    $content['created'] = array(
      '#type' => 'item',
      '#title' => t('Date'),
      '#markup' => format_date($installment->created, 'short')
    );

    $content['commerce_order'] = entity_build_content('commerce_order', $order, $view_mode, $langcode);

    return parent::buildContent($installment, $view_mode, $langcode, $content);
  }
}
      
      
