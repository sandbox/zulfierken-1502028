<?php

/**
 * @file
 * Page callbacks and form builder functions for administering installments.
 */


/**
 * Form callback: edit the global installment settings.
 */

function commerce_order_installment_edit_form($form, &$form_state,$order_id) {

$form['order_id']= array(
'#type'=> 'hidden',
'#value'=> $order_id,
 );  
 
$order = commerce_order_load($order_id);
$user = user_load($order->uid);

$balance = commerce_payment_order_balance($order);

$form['balance']= array(
'#type'=> 'hidden',
'#value'=> $balance['amount'],
 );  




$order_total = entity_metadata_wrapper('commerce_order', $order)->commerce_order_total->value();

$order_total_formatted = $order_total['currency_code'].' '.number_format($order_total['amount']/100, 2, '.', '');
$balance_formatted = $balance['currency_code'].' '.number_format($balance['amount']/100, 2, '.', '');

$installments = commerce_order_installment_all_unpaid_installments_of_order($order_id);

if(!empty($installments)) {

if ($balance['amount'] == 0) {

$form['message']= array(
'#type'=> 'fieldset',
'#title'=> t('This order is fully paid. Go to the <a href="@view-order">Order page to edit payments of this order. </a> .', array('@view-order' => url('admin/commerce/orders/'.$order_id.''))),
);
return $form;

}

}


if(empty($installments)) {
	
	$form['message1']= array(
'#type'=> 'fieldset',

'#title'=> t('There are no/unpaid installments created for this order.'),
);

if ($balance['amount'] == 0) {

$form['message2']= array(
'#type'=> 'fieldset',
'#title'=> t('This order is fully paid. Go to the <a href="@view-order">Order page to edit payments of this order. </a> .', array('@view-order' => url('admin/commerce/orders/'.$order_id.''))),
);
return $form;

}

}


$form['message']= array(
'#type'=> 'fieldset',
'#title'=> t('Add/Edit/Delete installments that belongs to '.$user->name.'. This form will validate the total unpaid installment and order balance. You may edit order and paid installments with clicking order and paid installment links.'),
);




// Edit Installments If there are initial installments

if(!empty($installments)) {


$form['create_order']['payments']['old_installments'] = array( 
 '#title' => t('Edit Installments that are not paid. Installment will be deleted if you leave amount as zero'),
 '#prefix' => '<div id="old_installments">',
  '#suffix' => '</div>',
  '#tree' => TRUE,
  '#theme' => 'table',
  '#header' => array(t('In. Number'), t('Amount'), t('Due Date'), t('Notes')),
  '#rows' => array(),
);

      
//   $form['create_order']['payments']['old_installments']['#tree'] = TRUE;


$b = 1;

foreach($installments as $installment){
	
	if(empty($installment->transaction_id)){
	$b++;
	
		
       
	$in_id = array(
    '#id' => 'old_installment-' . $b . '-installment_id',
'#type' => 'hidden',
'#default_value' => $installment->installment_id,
  );
  
	$in_number = array(
    '#id' => 'old_installment-' . $b . '-in.Number',
'#type' => 'textfield',
'#size' => 15,
'#default_value' => $installment->installment_number,
  );
  
  $amount = array(
    '#id' => 'old_installment-' . $b . '-amount',
'#type' => 'textfield',
'#size' => 15,
'#default_value' => $installment->amount/100,
  );
  
  $due_date = array(
    '#id' => 'old_installment-' . $b . '-due_date',
'#type' => 'date_popup',
'#size' => 15,
'#date_format' => 'Y-m-d',
       '#default_value' => date('Y-m-d', $installment->due_date),
       '#date_label_position' => 'none',
       // I hacked the date_popup module with adding if (!isset($element['#date_format_display']) || $element['#date_format_display']) { } to E.g., part to make sure no element is shown
       '#date_format_display' => FALSE,
       
  );
  
   $notes = array(
    '#id' => 'old_installment-' . $b . '-notes',
'#type' => 'textfield',
'#size' => 80,
'#default_value' => $installment->notes,
  );
  
  $form['create_order']['payments']['old_installments'][] = array(
    'in_number' => &$in_number,
    'installment_id' => &$in_id,
    'amount' => &$amount,
    'due_date' => &$due_date,
    'notes' => &$notes,
  
  );
  
   $form['create_order']['payments']['old_installments']['#rows'][] = array(
    array('data' => &$in_number),
    array('data' => &$amount),
    array('data' => &$due_date),
    array('data' => &$notes),

  );
  
   unset($in_number);
   unset($in_id);
   unset($amount);
   unset($due_date);
   unset($notes);
   
   
 
	
	
  
}
}	
	
}
// Add Installments If there were no installments initially


// Gets the default installment number if commerce_school_fee module is installed

if(empty($installments)) {


	
if(!isset($default_no_of_installment))
$default_no_of_installment = variable_get('commerce_order_installment_no_of_installment',1);



$num_installment_variable = 'commerce_order_installment_order_id_for_add_installment'.$order_id;

//  drupal_set_message('<pre>' . print_r($order_variable, true) . '</pre>');	

  if (empty($form_state['num_installments'])) {

$num_installments =variable_get($num_installment_variable,0);

if($num_installment_variable == 0)
	$num_installments = $default_no_of_installment;

    $form_state['num_installments'] = $num_installments;
    
    variable_set($num_installment_variable,$form_state['num_installments']);
  }

}

if(!empty($installments)) {

$num_installment_variable = 'commerce_order_installment_order_id_for_add_installment'.$order_id;


if (empty($form_state['num_installments'])) {
		
	$form_state['num_installments'] = 1;

	variable_set($num_installment_variable,$form_state['num_installments']);
	
}

}



$form['create_order']['payments']['new_installments'] = array( 
 '#title' => t('Add Installments. Installment will be deleted if you leave amount as zero'),
 '#prefix' => '<div id="new_installments">',
  '#suffix' => '</div>',
  '#tree' => TRUE,
  '#theme' => 'table',
  '#header' => array(t('In. Number'), t('Amount'), t('Due Date'), t('Notes')),
  '#rows' => array(),
);

      
//   $form['create_order']['payments']['old_installments']['#tree'] = TRUE;


 for ($i = 1; $i <= $form_state['num_installments']; $i++) {

$installment_number = 'Installment - '.($i);
if(!empty($installments))	
$installment_number = 'Installment - '.($i+$b - 1);

	$nin_number = array(
    '#id' => 'new_installment-' . $i . '-in.Number',
'#type' => 'textfield',
'#size' => 15,
'#default_value' => $installment_number,
  );
  
  $namount = array(
    '#id' => 'new_installment-' . $i . '-amount',
'#type' => 'textfield',
'#size' => 15,

  );
  
  $ndue_date = array(
    '#id' => 'new_installment-' . $i . '-due_date',
'#type' => 'date_popup',
'#size' => 15,
'#date_format' => 'Y-m-d',
       '#default_value' => date('Y-m-d'),
       '#date_label_position' => 'none',
       // I hacked the date_popup module with adding if (!isset($element['#date_format_display']) || $element['#date_format_display']) { } to E.g., part to make sure no element is shown
       '#date_format_display' => FALSE,
       
  );
  
   $nnotes = array(
    '#id' => 'new_installment-' . $i . '-notes',
'#type' => 'textfield',
'#size' => 80,

  );
  
  $form['create_order']['payments']['new_installments'][] = array(
    'in_number' => &$nin_number,
    'installment_id' => &$nin_id,
    'amount' => &$namount,
    'due_date' => &$ndue_date,
    'notes' => &$nnotes,
  
  );
  
   $form['create_order']['payments']['new_installments']['#rows'][] = array(
    array('data' => &$nin_number),
    array('data' => &$namount),
    array('data' => &$ndue_date),
    array('data' => &$nnotes),

  );
  
   unset($nin_number);
   unset($nin_id);
   unset($namount);
   unset($ndue_date);
   unset($nnotes);

}

 $form['add_installment'] = array(
    '#type' => 'submit',
    '#value' => t('Add More'),
    '#submit' => array('commerce_order_installment_ui_add_more_installment'),
     '#limit_validation_errors' => array(),
 
  );
  
if ($form_state['num_installments'] > 1) {
    $form['remove_installment'] = array(
      '#type' => 'submit',
      '#value' => t('Remove Latest'),
      '#submit' => array('commerce_order_installment_ui_remove_one_installment'),
      // Since we are removing a name, don't validate until later.
      '#limit_validation_errors' => array(),
      
    );
  }


$form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save All Installments'),
    '#submit' => array('commerce_order_installment_ui_edit_submit'),
    '#validate' => array('commerce_order_installment_ui_edit_validate'),
    
  );
$form['deletenote'] = array(
    '#type' => 'item',
    '#markup' => 'This will delete all installments without validation! Click if you want to delete all installments only!',
    
    
  );

$form['delete'] = array(
    '#type' => 'submit',
    '#title' => 'This will delete all installments without validation!',
    '#value' => t('Delete All Installments'),
    '#submit' => array('commerce_order_installment_ui_delete_submit'),
    '#validate' => array('commerce_order_installment_ui_delete_validate'),
    
  );
 


// Line Item Table

$line_items_embedded_view = views_embed_view('line_items_of_an_order', 'default', $order_id);
    if (!empty($line_items_embedded_view)) {

      // hide line item form and show summary view display
      
      $form['lineitem_summary'] = array(
        '#type' => 'item',
        '#title' => t('Order Summary'),
        '#description' => '',
        '#markup' => $line_items_embedded_view,
        '#weight' => 50,
      );
    }

        
$form['order_total'] = array(
        '#type' => 'item',
        '#title' => t('Total:'),
        
        '#markup' => $order_total_formatted,
        '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
'#weight' => 51,
      );
      
$form['order_balance'] = array(
        '#type' => 'item',
        '#title' => t('Balance:'),
        
        '#markup' => $balance_formatted,
        '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
'#weight' => 52,
      );

// Paid installments table This field is filtered by transaction id. Therefore, you must update the installment and delete the value in transaction id cell to change installment to unpaid installment.

$paid_installments_embed_view = views_embed_view('paid_order_installments', 'page_2', $order_id);
	  
	  	if(!isset($paid_installments_embed_view)) {
	  	$form['paid_installments'] = array(
        '#type' => 'item',
        '#title' => t('Paid Installments: You have to delete related transaction before deleting or editing this installment'),
        '#description' => '',
        '#markup' => $paid_installments_embed_view,
        '#weight' => 60,
      );
    }
    
return $form;
}

function commerce_order_installment_select_form($form, &$form_state) {

drupal_goto('admin/commerce/installment/edit');	 
	  	
	  	
	  	
	  	
return $form;
}





function commerce_order_installment_settings_form($form, &$form_state) {
  $form['commerce_order_installment_no_of_installment'] = array(
    '#type' => 'select',
    '#title' => t('This form can be used to set default number of installments'),
    
    '#default_value' => variable_get('commerce_order_installment_no_of_installment', 1),
    '#options' => array(
      1 => 1,
      2 => 2,
      3 => 3,
      4 => 4,
      5 => 5,
      6 => 6,
      7 => 7,
      8 => 8,
      9 => 9,
      10 => 10,
      11 => 11,
      12 => 12,
      
    )
  );

  return system_settings_form($form);
}


function commerce_order_installment_ui_edit_submit($form, &$form_state) {

	 //	drupal_set_message('<pre>' . print_r($form_state['values'], true) . '</pre>');

$order = commerce_order_load($form_state['values']['order_id']);

$now = REQUEST_TIME;

$wrapper = entity_metadata_wrapper('commerce_order', $order);
$currency_code = $wrapper->commerce_order_total->currency_code->value();

global $user;
$added_by = $user->uid;
$status = 'Not Paid';


//Update old installments
if(isset($form_state['values']['old_installments'])) {

	$old_installments = array_filter($form_state['values']['old_installments'], 'intval');


foreach ($old_installments as $installmentinstallments) {



if (!empty($installmentinstallments['amount'])){

$installment_number = $installmentinstallments['in_number']; 
$amount = $installmentinstallments['amount']*100;
$due_date = strtotime($installmentinstallments['due_date']);

$installment = commerce_order_installment_load($installmentinstallments['installment_id']);
$installment->installment_number = $installment_number;
$installment->amount = $amount;
$installment->due_date = $due_date;
$installment->notes = $installmentinstallments['notes'];
$installment->added_by = $added_by;
$installment->changed = $now;


commerce_order_installment_save($installment);


}   

if (empty($installmentinstallments['amount']))
commerce_order_installment_delete($installmentinstallments['installment_id']);

if ($installmentinstallments['amount'] == 0)
commerce_order_installment_delete($installmentinstallments['installment_id']);

}

}
//Add new installments


	$new_installments = array_filter($form_state['values']['new_installments'], 'intval');


foreach ($new_installments as $installmentinstallments) {



if (!empty($installmentinstallments['amount'])){

$installment_number = $installmentinstallments['in_number']; 
$amount = $installmentinstallments['amount']*100;
$due_date = strtotime($installmentinstallments['due_date']);

$installment = commerce_order_installment_new();
$installment->installment_number = $installment_number;
$installment->amount = $amount;
$installment->due_date = $due_date;
$installment->notes = $installmentinstallments['notes'];
$installment->status = $status;
$installment->currency = $currency_code;
$installment->order_id = $order->order_id;
$installment->uid = $order->uid;
$installment->added_by = $added_by;


commerce_order_installment_save($installment);


}   
}


  	//drupal_set_message('<pre>' . print_r($installment, true) . '</pre>');
}

function commerce_order_installment_ui_edit_validate($form, &$form_state) {

$request_parts = explode('/', $_SERVER['REQUEST_URI']);
$order_id_from_url = intval(array_pop($request_parts));
$order_id = $form_state['values']['order_id'];
$balance = $form_state['values']['balance'] / 100;
if($order_id_from_url != $order_id)
form_error($form['message'], t('Order id of url and order does not match. I do not know how you achieved but you cannot submit this form!.'));
	  	


 

// Sum up old installments
$sum_old_installments = array();
if(isset($form_state['values']['old_installments'])) {
	
 foreach($form_state['values']['old_installments'] as $k => $old_installment) {
 	
 	foreach ($old_installment as $id => $value) {
 
 if (array_key_exists($id, $sum_old_installments)) { 
 		$sum_old_installments[$id]+= $value;
 	}
 	Else {
 		$sum_old_installments[$id]= $value;
 	}
 	}
 	
}
}
// Sum up new installments

$sum_new_installments = array();

 foreach($form_state['values']['new_installments'] as $k => $new_installment) {
 	
 	foreach ($new_installment as $id => $value) {
 
 if (array_key_exists($id, $sum_new_installments)) { 
 		$sum_new_installments[$id]+= $value;
 	}
 	Else {
 		$sum_new_installments[$id]= $value;
 	}
 	}
 	
}
if(!isset($form_state['values']['old_installments']))

$sum_old_installments['amount'] = 0;

$installment_sum = $sum_old_installments['amount'] + $sum_new_installments['amount'];
if( $balance != $installment_sum)
form_error($form['message'], t('Order Balance('.$balance.') and Total Installment ('.$installment_sum.')does not mach! Please edit installments so that they can mach!.'));

 
  
}


function commerce_order_installment_ui_add_more_installment($form, &$form_state) {
  // Everything in $form_state is persistent, so we'll just use
  // $form_state['add_name']



$order_id = $form_state['input']['order_id'];
  
  $form_state['num_installments']++;

$num_installment_variable = 'commerce_order_installment_order_id_for_add_installment'.$order_id;

variable_set($num_installment_variable,$form_state['num_installments']);
  // Setting $form_state['rebuild'] = TRUE causes the form to be rebuilt again.
  $form_state['rebuild'] = TRUE;
}




function commerce_order_installment_ui_remove_one_installment($form, &$form_state) {

	  	
  $order_id = $form_state['input']['order_id'];

$num_installment_variable = 'commerce_order_installment_order_id_for_add_installment'.$order_id;
  
  if ($form_state['num_installments'] > 1) {
    $form_state['num_installments']--;
    variable_set($num_installment_variable,$form_state['num_installments']);
  }

  // Setting $form_state['rebuild'] = TRUE causes the form to be rebuilt again.
  $form_state['rebuild'] = TRUE;
}

function commerce_order_installment_ui_delete_submit($form, &$form_state) {

$old_installments = array_filter($form_state['values']['old_installments'], 'intval');


foreach ($old_installments as $installmentinstallments) {



//drupal_set_message('<pre>' . print_r($installmentinstallments, true) . '</pre>');	

	commerce_order_installment_delete($installmentinstallments['installment_id']);
}
	
}

function commerce_order_installment_ui_delete_validate($form, &$form_state) {

$request_parts = explode('/', $_SERVER['REQUEST_URI']);
$order_id_from_url = intval(array_pop($request_parts));
$order_id = $form_state['values']['order_id'];
if($order_id_from_url != $order_id)
form_error($form['message'], t('Order id of url and order does not match. I do not know how you achieved but you cannot submit this form!.'));
	  	
}


function commerce_order_add_installment_select_form($form, &$form_state) {



drupal_goto('admin/commerce/installment/add');	 
	  	
return $form;
}



function commerce_order_pay_installment_complete($form, &$form_state) {


$form['filter'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Choose a user.'), 
    '#attributes' => array('class' => array('container-inline')), 
  );
$form['filter']['select_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Choose User'),
    '#autocomplete_path' => 'user/autocomplete',
    
  );
 
 $uid = Null;
 if(isset($form_state['input']['select_user'])){
 
 	$user = user_load_by_name($form_state['input']['select_user']);
 	$uid = $user->uid;
}

  $form['filter']['apply_filter'] = array(
    '#type' => 'submit',
    '#value' => t('Apply Filter'),
    '#submit' => array('commerce_order_installment_ui_pay_installment_chooser_submit'),
    
    
  );
  

$header = array(
    'id' => array('data' => t('Id')),
    'order_id' => array('data' => t('Order ID'), 'sort' => 'desc'),
    'installment_number' => array('data' => t('Installment Number'), 'sort' => 'desc'),
    'amount' => array('data' => t('Amount')),
    'due_date' => array('data' => t('Due Date')),
    'status' => array('data' => t('Status')),
        'notes' => array('data' => t('Notes')),
 
  );


$options = commerce_order_installment_ui_pay_installment_table_select($uid);


$form['unpaid_instalments'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Choose all installments from same order. You cannot choose installments from different orders!'), 
    '#attributes' => array('class' => array('container-inline')), 
  );
  
   $form['unpaid_instalments']['table'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No content available.'),
    );
if (module_exists('commerce_school_fee')){
$form['payments'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Create transaction. Use [Installment-Number] token in description if you want to change transaction description but shown installment number in receipt'), 
    '#attributes' => array('class' => array('container-inline')), 
  );
  
  $form['payments']['amount'] = array(
          '#type' => 'textfield',
          '#title' => t('Confirm Amount'),
           '#size' => 15,
           '#attributes' => array(
        'placeholder' => t('Total Amount'),
    ),
    );
    
  $form['payments']['paid_notes'] = array(
          '#type' => 'textfield',
          '#title' => t('Description'),
           '#size' => 50,
           '#attributes' => array(
        'placeholder' => t('[Installment-Number]Payment Received Thank You'),
    ),
    );



$form['payments']['paid_by'] = array(
          '#type' => 'select',
          '#title' => t('Method'),
           '#size' => 1,
           '#options' => array(
          'commerce_school_fee_payment_cash' => t('Cash'),
          'commerce_school_fee_payment_bank' => t('Bank'),
          'commerce_school_fee_payment_cheque' => t('Cheque'),
  
       ),
           
    );
 $form['payments']['method_remark'] = array(
          '#type' => 'textfield',
          '#title' => t(''),
           '#size' => 15,
           '#attributes' => array(
        'placeholder' => t('Remark'),
    ),
    );
  }
  
  
  
  if (!module_exists('commerce_school_fee')){
  
  $form['payments'] = array(
    '#type' => 'fieldset', 
    '#title' => t('You should enter transaction before. This form will submit transaction id to installments and mark them as paid!'), 
    '#attributes' => array('class' => array('container-inline')), 
  );
  
  $form['payments']['amount'] = array(
          '#type' => 'textfield',
          '#title' => t('Confirm Amount'),
           '#size' => 15,
           '#attributes' => array(
        'placeholder' => t('Total Amount'),
    ),
    );
    
  $form['payments']['transaction_id'] = array(
          '#type' => 'textfield',
          '#title' => t('Transaction ID'),
           '#size' => 20,
           '#attributes' => array(
        'placeholder' => t('Write down the transaction IDs that these installment paid with'),
    ),
    );
  
}
   $form['payments']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Pay Installments'),
    '#submit' => array('commerce_order_installment_ui_pay_installment_submit'),
    '#validate' => array('commerce_order_installment_ui_pay_installment_validate'),
    
  );

  	
	  		  	  		  		  	  	
	 
  if($uid != Null) {
$paid_installments_view = views_embed_view('paid_order_installments', 'page_1', $uid);
    

      $form['paidinstallment_summary'] = array(
        '#type' => 'item',
        '#title' => t('Paid Installment Summary'),
        '#description' => '',
        '#markup' => $paid_installments_view,
        '#weight' => 50,
      );
    }


return $form;
}

function commerce_order_installment_ui_pay_installment_table_select ($uid){
	
	$query = new EntityFieldQuery();
  
  $result = $query
    ->entityCondition('entity_type', 'commerce_order_installment')
    ->propertyCondition('transaction_id',0, '<=')
    ->propertyCondition('uid',$uid, '=')
    ->execute();


$options = array();
 foreach ($result as $installments) {

foreach ($installments as $installment) {

$il = commerce_order_installment_load($installment ->installment_id);

$options[$il->installment_id] = array(
'id' => $il->uid,
'order_id' => $il->order_id,
'installment_number' => $il->installment_number,
'amount' => $il->amount/100,
'due_date' => date('d-M-Y',$il->due_date),
'status' => $il->status,
'notes' => $il->notes,
);

}
}
	return $options;
}



//User Chooser/Apply Filter
function commerce_order_installment_ui_pay_installment_chooser_submit($form, &$form_state) {
	
	$form_state['rebuild'] = TRUE;
}

function commerce_order_installment_ui_pay_installment_submit($form, &$form_state) {


	// Create transaction
$installment_number ='';
$installments_paid = array_filter($form_state['values']['table'], 'strlen');
foreach($installments_paid as $installment_paid) {

		if($installment_paid != 0) {
		$installment = commerce_order_installment_load($installment_paid);
		$installment_number.= $installment->installment_number.',';
	}
}
$order = commerce_order_load($installment->order_id);
$invoice_id = $order->field_invoice_id['und']['0']['value'];



$wrapper = entity_metadata_wrapper('commerce_order', $order);
$currency_code = $wrapper->commerce_order_total->currency_code->value();
$paid_amount = $form_state['values']['amount']*100;	

if (module_exists('commerce_school_fee')){
	// If School Fee module is installed !! If it is not installed use an alternative payment method with hardcoding
	$payment_method = commerce_payment_method_load($form_state['values']['paid_by']);
	$how_paid = $payment_method['title'].' & '.$form_state['values']['method_remark'];
	if (empty($form_state['values']['method_remark']))
$how_paid = $payment_method['title'];
$payment_method['instance_id']= $payment_method['method_id'] . '|' . "commerce_payment_" . $payment_method['method_id'];
$charge = array(
        'amount' => $paid_amount, 
        'currency_code' => $currency_code,
      );
$transaction = commerce_school_fee_payment_transaction($payment_method, $order, $charge);       
}

if (!module_exists('commerce_school_fee')){
	$transaction->transaction_id = $form_state['values']['transaction_id'];
}
// Update Installments as Paid

$now = REQUEST_TIME;
$instalment_status = t('Paid');
$installments_paid = array_filter($form_state['values']['table'], 'strlen');
foreach($installments_paid as $installment_paid) {

		if($installment_paid != 0) {
		$installment = commerce_order_installment_load($installment_paid);

$installment->paid_date = $now;
$installment->status = $instalment_status;
$installment->transaction_id = $transaction->transaction_id;
commerce_order_installment_save($installment);


	}
}
  if (module_exists('commerce_school_receipt')){ 
  
// Create a receipt I assume that receipt module is installed

global $user;
$added_by = $user->uid;
$receipt_status = t('active');


$receipt = commerce_school_receipt_new();

$receipt ->uid = $order->uid;
$receipt ->order_id = $order->order_id;
$receipt ->amount = $paid_amount;
$receipt ->notes = $form_state['values']['paid_notes'];


$note_check = explode('[INSTALLMENT-NUMBER]', $receipt ->notes);

if ($note_check[1] !=Null)  
$receipt ->notes = $installment_number.$note_check[1];
 
if($receipt ->notes == '[INSTALLMENT-NUMBER]')
$receipt ->notes = $installment_number;

if($receipt ->notes == '')
$receipt ->notes = $installment_number.'Payment Received Thank You';

$receipt ->added_by = $added_by;
$receipt ->transaction_id = $transaction->transaction_id;
$receipt ->status = $receipt_status;
$receipt ->how_paid = $how_paid;


$receipt = commerce_school_receipt_save($receipt);

$receipt_id = $receipt->receipt_id;

$balance = commerce_payment_order_balance($order);

  // If the balance was incalculable, set the balance to the order total.
  if ($balance === FALSE) {
    $balance = entity_metadata_wrapper('commerce_order', $order)->commerce_order_total->value();
  }

 if ($balance['amount'] == 0)   
commerce_order_status_update($order,'school_fee_completed');

drupal_goto('commerce_school_fees/fee_receipt/'.$order->order_id.'/'.$order->uid.'/'.$receipt_id.'/');	 

}
}

function commerce_order_installment_ui_pay_installment_validate($form, &$form_state) {


	
	$installments_paid = array_filter($form_state['values']['table'], 'strlen');

$order_check = Null;
$total_installment_amount = 0;
foreach($installments_paid as $installment_paid) {
	
	
		if($installment_paid != 0) {
		$installment = commerce_order_installment_load($installment_paid);
		
		if($order_check == Null)
		$order_check = $installment->order_id;
		$total_installment_amount = $total_installment_amount + $installment ->amount;
if($order_check != $installment->order_id)
form_error($form['unpaid_instalments'], t('You have to choose all installments from same order!!.'));

if($installment->transaction_id > 0)
form_error($form['unpaid_instalments'], t('This installment is paid before!!.'));

}
}

if( $total_installment_amount /100 != $form_state['values']['amount'])
	form_error($form['payments'], t('Total amount is not same as total installment!!.'));



}