<?php

/**
 * Filter by installment status.
 */
class commerce_order_installment_handler_filter_instalment_status extends views_handler_filter_in_operator {
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Status');
      $this->value_options = commerce_order_installment_status_get();
    }
  }
}
