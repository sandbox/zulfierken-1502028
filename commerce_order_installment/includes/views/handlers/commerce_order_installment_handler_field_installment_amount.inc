<?php

/**
 * Defines an order total area handler that shows the order total field with its
 *   components listed in the footer of a View.
 */
class commerce_order_installment_handler_field_installment_amount extends views_handler_field {
function init(&$view, &$options) {
    parent::init($view, $options);

  }

  function option_definition() {
    $options = parent::option_definition();

    $options['link_to_installment'] = array('default' => 'none');

    return $options;
  }

  /**
   * Provide the link to installment option.
   */
  

  /**
   * Render whatever the data is as a link to the installment.
   *
   * Data should be made XSS safe prior to calling this function.
   */
  function render_link($data, $values) {
    if (!empty($this->options['link_to_installment']) && in_array($this->options['link_to_installment'], array('customer', 'admin')) && $data !== NULL && $data !== '') {
      $uid = $this->get_value($values, 'uid');
      $currency = $this->get_value($values, 'currency');
      $installment_id = $this->get_value($values, 'installment_id');
      $order_id = $this->get_value($values, 'order_id');

      if ($this->options['link_to_installment'] == 'customer' && $uid) {
        $this->options['alter']['make_link'] = TRUE;
        $this->options['alter']['path'] = 'user/' . $uid . '/installments/' . $installment_id;
      }
      elseif ($this->options['link_to_installment'] == 'admin') {
        $this->options['alter']['make_link'] = TRUE;
        $this->options['alter']['path'] = 'admin/commerce/orders/' . $order_id . '/installment';
      }
    }

    return $data;
  }

  function render($values) {
    $installmentload = commerce_order_installment_load($values->commerce_order_installment_installment_id);
      $currency = $installmentload->currency;
    
    
    $value = $this->get_value($values) / 100;
    
    return ($currency. ' ' .number_format($value,2,'.',','));
  }
}
