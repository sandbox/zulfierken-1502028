<?php

/**
 * Field handler to translate an installment type into its readable form.
 */
class commerce_order_installment_handler_field_installment_type extends views_handler_field {
  function render($values) {
    $type = $this->get_value($values);
    if ($type) {
      return commerce_installment_type_get_name($type);
    }
  }
}
