<?php

/**
 * Export Drupal Commerce installment to Views.
 */

/**
 * Implements hook_views_data()
 */
function commerce_order_installment_views_data() {
  $data = array();

  $data['commerce_order_installment']['table']['group']  = t('Commerce Installment');

  $data['commerce_order_installment']['table']['base'] = array(
    'field' => 'installment_number',
    'title' => t('Commerce Installment'),
    'help' => t('Installments generated for an order.'),
  );
  
  // Expose the installment ID.
  $data['commerce_order_installment']['installment_id'] = array(
    'title' => t('Installment ID', array(), array('context' => 'a drupal commerce installment')),
    'help' => t('The unique internal identifier of the installment.'),
    'field' => array(
      'handler' => 'commerce_order_installment_handler_field_installment',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      /*'name field' => 'order_number',
      'numeric' => TRUE,
      'validate type' => 'order_id',*/
    ),
  );
  
  // Expose the installment number.
  $data['commerce_order_installment']['installment_number'] = array(
    'title' => t('Installment number', array(), array('context' => 'a drupal commerce installment')),
    'help' => t('The unique installment number.'),
    'field' => array(
      'handler' => 'commerce_order_installment_handler_field_installment',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  // Expose the order id
  $data['commerce_order_installment']['order_id'] = array(
    'title' => t('Order id'),
    'help' => t('The order id associated to the installment.'),
    'relationship' => array(
      'title' => t('Order'),
      'help' => t("Relate this installment to its order"),
      'handler' => 'views_handler_relationship',
      'base' => 'commerce_order',
      'base field' => 'order_id',
      'field' => 'order_id',
      'label' => t('Order associated to the installment'),
    ),
  );
  
  // Expose the installment type.
  $data['commerce_order_installment']['type'] = array(
    'title' => t('Installment type', array(), array('context' => 'a drupal commerce installment')),
    'help' => t('The type of the installment.'),
    'field' => array(
      'handler' => 'commerce_order_installment_handler_field_installment_type',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'commerce_order_installment_handler_filter_installment_type',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  // Expose the owner uid.
  $data['commerce_order_installment']['uid'] = array(
    'title' => t('Uid'),
    'help' => t("The owner's user ID."),
    'field' => array(
      'handler' => 'views_handler_field_user',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
      'name field' => 'name', // display this field in the summary
    ),
    'filter' => array(
      'title' => t('Name'),
      'handler' => 'views_handler_filter_user_name',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'title' => t('Owner'),
      'help' => t("Relate this installment to its owner's user account"),
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'field' => 'uid',
      'label' => t('Installment owner'),
    ),
  );
  
  // Expose the created timestamp.
  $data['commerce_order_installment']['created'] = array(
    'title' => t('Created date'),
    'help' => t('The date the installment was created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  
 
$data['commerce_order_installment']['amount'] = array(
    'title' => t('Amount'),
    'help' => t('The Installment amount that will be paid with this installment.'),
    'field' => array(
      'handler' => 'commerce_order_installment_handler_field_installment_amount',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
$data['commerce_order_installment']['due_date'] = array(
    'title' => t('Due date'),
    'help' => t('The date that the installment will be paid.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  $data['commerce_order_installment']['paid_date'] = array(
    'title' => t('Paid Date'),
    'help' => t('The date that the installment was paid.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  $data['commerce_order_installment']['status'] = array(
    'title' => t('Status'),
    'help' => t('Status of the installment (Paid or not paid).'),
    'field' => array(
      'handler' => 'commerce_order_installment_handler_field_installment',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'commerce_order_installment_handler_filter_instalment_status',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  $data['commerce_order_installment']['notes'] = array(
    'title' => t('Notes'),
    'help' => t('Notes about the installment.'),
    'field' => array(
      'handler' => 'commerce_order_installment_handler_field_installment',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['commerce_order_installment']['transaction_id'] = array(
    'title' => t('Transaction Id'),
    'help' => t('Transaction Id for Paid installment.'),
    'field' => array(
      'handler' => 'commerce_order_installment_handler_field_installment',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  $data['commerce_order_installment']['added_by'] = array(
    'title' => t('Added By'),
    'help' => t("User ID of the person that added this installment."),
    'field' => array(
      'handler' => 'views_handler_field_user',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
      'name field' => 'name', // display this field in the summary
    ),
    'filter' => array(
      'title' => t('Name'),
      'handler' => 'views_handler_filter_user_name',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'title' => t('Added by'),
      'help' => t("Relate this installment to creator's user account"),
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'field' => 'added_by',
      'label' => t('Installment Added by'),
    ),
  );
  
 
  $data['commerce_order_installment']['currency'] = array(
    'title' => t('Currency'),
    'help' => t('Currency about the installment.'),
    'field' => array(
      'handler' => 'commerce_order_installment_handler_field_installment',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  
  // buraya kadar
  return $data;
}

/**
 * Implements hook_views_data_alter()
 */
function commerce_order_installment_views_data_alter(&$data) {
  $data['commerce_order']['installment_id'] = array(
    'title' => t('Installment id'),
    'help' => t('The installment id associated with this order.'),
    'relationship' => array(
      'title' => t('Installments'),
      'help' => t("Relate this order to its installment"),
      'handler' => 'views_handler_relationship',
      'base' => 'commerce_order_installment',
      'base field' => 'order_id',
      'relationship field' => 'order_id',
      'field' => 'order_id',
      'label' => t('Installment associated to the order'),
    ),
  );
}

