<?php

/**
 * @file
 * Builds placeholder replacement tokens for installment-related data.
 */


/**
 * Implements hook_token_info().
 */
function commerce_order_installment_token_info() {
  $type = array(
    'name' => t('Installments', array(), array('context' => 'a drupal commerce installment')),
    'description' => t('Tokens related to individual installments.'),
    'needs-data' => 'commerce-order-installment',
  );

  // Tokens for installments.
  $installment = array();

  $installment['installment-id'] = array(
    'name' => t('Installment ID', array(), array('context' => 'a drupal commerce installment')),
    'description' => t('The unique numeric ID of the installment.'),
  );
  
  $installment['installment-number'] = array(
    'name' => t('Installment number', array(), array('context' => 'a drupal commerce installment')),
    'description' => t('The installment number displayed to the customer.'),
  );
  
  // Refer to corresponding order from installment
  $installment['order'] = array(
    'name' => t('Order'),
    'description' => t('The order associated to the installment.'),
    'type' => 'commerce-order',
  );
  
  return array(
    'types' => array('commerce-order-installment' => $type),
    'tokens' => array('commerce-order-installment' => $installment),
  );
}

/**
 * Implements hook_tokens().
 */
function commerce_order_installment_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $url_options = array('absolute' => TRUE);

  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
    $language_code = $options['language']->language;
  }
  else {
    $language_code = NULL;
  }

  $sanitize = !empty($options['sanitize']);

  $replacements = array();

  if ($type == 'commerce-order-installment' && !empty($data['commerce-order-installment'])) {
    $installment = $data['commerce-order-installment'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        // Simple key values on the installment.
        case 'installment-id':
          $replacements[$original] = $installment->installment_id;
          break;

        case 'installment-number':
          $replacements[$original] = $sanitize ? check_plain($installment->installment_number) : $installment->installment_number;
          break;

        case 'order':
          if ($installment->order_id) {
            $order = commerce_order_load($installment->order_id);
            $replacements[$original] = $sanitize ? check_plain($order->order_number) : $order->order_number;
          }
          break;
      }
    }

    if ($order_tokens = token_find_with_prefix($tokens, 'order')) {
      $order = commerce_order_load($installment->order_id);
      $replacements += token_generate('commerce-order', $order_tokens, array('commerce-order' => $order), $options);
    }
  }

  return $replacements;
}
